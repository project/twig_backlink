<?php
//#Drush auto-generate all file

namespace Drupal\twig_backlink;

use Collator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * This twig extension adds a new twig_backlink function to build a list of
 * links of the parent entities.
 */
class TwigBacklinkTwigExtension extends AbstractExtension {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TwigBacklinkTwigExtension object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(RouteMatchInterface $route_match, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->routeMatch = $route_match;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('twig_backlink', [$this, 'twigBacklink']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'twig_backlink';
  }

  /**
   * Returns the render array of parents nodes.
   *
   * @param string $field_name
   *   The field name.
   *
   * @return null|array
   *   A render array for the field or NULL if the value does not exist.
   */
  public function twigBacklink($field_name): ?array {
    $current_nid = $this->routeMatch->getParameter('node');
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if (!is_null($current_nid)) {
      $nid = $current_nid->nid->value;
    }
    else {
      $nid = '';
    }

    $links = [];

    // Get the list of node ids.
    $nids = $this->getNids($nid, $field_name);

    // Get the label from the settings.
    $config = $this->configFactory->get('twig_backlink.settings');
    $label = $config->get('label');

    if ($nids) {
      foreach ($nids as $nid) {

        $node = Node::load($nid);
        $options = ['attributes' => ['class' => 'backlink-link']];
        $links['backlink_links'][] = [
          '#title' => $node->label(),
          '#type' => 'link',
          '#url' => Url::fromRoute('entity.node.canonical',
            ['node' => $nid], $options)
        ];
      }
    }

    if (array_key_exists('backlink_links', $links)) {
      $links = _twig_backlink_array_sort($links['backlink_links'], '#title');
    }

    return [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => $label,
      '#items' => $links,
      '#attributes' => ['class' => ['backlink-list']],
    ];
  }

  /**
   * Returns the parent nid of the referenced nodes.
   *
   * @return array|int
   */
  public function getNids($nid, $field_name) {

    $query = $this->entityTypeManager->getStorage('node')->getQuery();

    return $query
      // Referenced ID
      ->accessCheck(FALSE)
      ->condition($field_name, $nid)
      ->condition('status', 1)
      ->execute();
  }
}


/**
 * Local sort function.
 *
 * @return array
 */
function _twig_backlink_array_sort($array, $on, $order=SORT_ASC): array {
  $new_array = array();
  $sortable_array = array();
  $lang_code = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $collator = new Collator($lang_code);

  if (count($array) > 0) {
    foreach ($array as $k => $v) {
      if (is_array($v)) {
        foreach ($v as $k2 => $v2) {
          if ($k2 == $on) {
            $sortable_array[$k] = $v2;
          }
        }
      } else {
        $sortable_array[$k] = $v;
      }
    }

    if ($order == SORT_ASC) {
      $collator->asort($sortable_array);
    }

    foreach ($sortable_array as $k => $v) {
      $new_array[$k] = $array[$k];
    }
  }

  return $new_array;
}
